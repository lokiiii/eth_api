const { Router } = require("express");
const router = Router();

const transaction = require("../services/transactionServices");

router.get("/", (req, res) => {
  console.log("Working");
});

router.post("/createWallet", async (req, res) => {
    let result = await transaction.createWallet();
    res.send(result);
});

router.post("/getBalance", async (req, res) => {
    let balance = await transaction.balanceMain(req.body.wallet);
    res.send(balance);
});

router.post("/transferETH", async (req, res) => {
    let hash = await transaction.ETHTransfer(req.body.from , req.body.to, req.body.amount, req.body.privateKey);
    res.send(hash);
});

router.post("/checkHash", async (req, res) => {
    let hash = await transaction.hashStatus(req.body.hash);
    res.send(hash);
});

module.exports = router;
