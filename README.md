**ETHEREUM API's**

To start the project follow the following commands :

Step 1 - Do a git clone of the project in your desired folder location.

Step 2 - Run the command npm i to install the packages in your local folder.

Step 3 - Run the command node app.js to start the project.

The project is started on localhost:8000

* You would need Node.js installed in your system to start this project.
