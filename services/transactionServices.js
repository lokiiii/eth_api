const web3 = require('web3');
const { Transaction } = require('ethereumjs-tx');

const web3js = new web3(
    new web3.providers.HttpProvider(
      "https://ropsten.infura.io/v3/9e6d530e9835469c845878123cbe3dd0"
    )
);

const createWallet = async () => {
    let newData = web3js.eth.accounts.create();
    if(newData){
        let valid_wallet = JSON.stringify(newData);
        return valid_wallet;
    }
}

const ETHTransfer =  async (sender_address, receiver_address, amount, sender_private_key) => {
    try {
        if(sender_private_key.length > 64){
            let num = sender_private_key.length - 64;
            sender_private_key = sender_private_key.slice(num);
        }
        const privateKey = Buffer.from(sender_private_key, 'hex');
        let estimates_gas = await web3js.eth.estimateGas({
            from: sender_address,
            to: receiver_address,
            amount: web3js.utils.toWei(amount, "ether"),
        });
        let gasLimit = web3js.utils.toHex(estimates_gas * 2);
        let gasPrice_bal = await web3js.eth.getGasPrice();
        let gasPrice = web3js.utils.toHex(gasPrice_bal * 2);
        let v = await web3js.eth.getTransactionCount(sender_address)
        let rawTransaction = {
            "gasPrice": gasPrice,
            "gasLimit": gasLimit,
            "to": receiver_address,
            "value": web3js.utils.toHex(web3js.utils.toWei(amount, "ether")),
            "nonce": web3js.utils.toHex(v),
        }
        let transaction = new Transaction(rawTransaction, {chain: 'ropsten'});
        transaction.sign(privateKey);
        let hash = await web3js.eth.sendSignedTransaction('0x' + transaction.serialize().toString('hex'));
        if(hash){
            let valid_pass = { success: 1, hash: hash.transactionHash };
            let valid_wallet = JSON.stringify(valid_pass);
            return valid_wallet;
        }
    } catch (error) {
        let valid_pass = { success: 0, msg: "The transaction is failed." };
        let valid_wallet = JSON.stringify(valid_pass);
        return valid_wallet;
    }
}

const hashStatus = async (hash) => {
    let status = await web3js.eth.getTransactionReceipt(hash);
    if(status){
        status = status.blockNumber;
        let valid_pass = { success: 1, hashStatus: "The transaction is successful." };
        let valid_wallet = JSON.stringify(valid_pass);
        return valid_wallet;
    }
    else{
        let valid_pass = { success: 0, msg: "The transaction is fail or pending." };
        let valid_wallet = JSON.stringify(valid_pass);
        return valid_wallet;
    }
}

const balanceMain = async (account) => {
    let balance = await web3js.eth.getBalance(account);
    if(balance){
        balance = balance/(Math.pow(10,18));
        let valid_pass = { success: 1, balance: balance };
        let valid_wallet = JSON.stringify(valid_pass);
        return valid_wallet;
    }
    else{
        let valid_pass = { success: 0, msg: "Error in finding balance." };
        let valid_wallet = JSON.stringify(valid_pass);
        return valid_wallet;
    }
};

module.exports = {
    createWallet,
    ETHTransfer,
    hashStatus,
    balanceMain
}